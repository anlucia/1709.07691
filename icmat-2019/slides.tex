\documentclass[10pt,compress]{beamer}
\usepackage{appendixnumberbeamer}
\usetheme{metropolis}

\usepackage{lmodern}
\renewcommand\textbullet{\ensuremath{\bullet}}
\usepackage{physics}

\usetikzlibrary{decorations.pathmorphing}
\tikzset{
  onslide/.code args={<#1>#2}{ \onslide<#1>{\pgfkeysalso{#2}} },
  tensor/.style={rectangle,draw=blue!50,fill=blue!20, thick},
  spin/.style={circle,draw=blue!60, fill=blue!50, scale=0.4},
  epr/.style={draw=magenta!70,-, decorate, decoration={snake, amplitude=0.05cm, segment
      length=0.1cm}},
  boundary/.style={draw=yellow!70,fill=yellow!40, thick}
}
% math fonts
\usepackage{dsfont}
\newcommand{\mcl}{\mathcal}

% fields and rings
\newcommand{\field}[1]{\ensuremath{\mathds{#1}}}
\newcommand{\hs}{\mcl H}
\newcommand{\matrixalg}{\mcl M}
\newcommand{\id}{\mathds{1}}
\newcommand{\traop}{\mathds{E}}

\DeclareMathOperator{\ima}{Im}
\DeclareMathOperator{\OExp}{Exp}
\DeclareMathOperator{\supp}{supp}
\DeclareMathOperator{\linspan}{span}

\graphicspath{{./figures/},{./../../}}

\title{Boundary Hamiltonians \\and spectral gaps \\for 2D PEPS  }
\author[A. Lucia]{\underline{Angelo Lucia} (Caltech)\\
  M.~J.~Kastoryano (University of Cologne)\\
  D.~Perez-Garcia (Universidad Complutense de Madrid)
}
\subtitle{\texttt{1709.07691} \\ {\small Commun. Math. Phys. (2019) 366: 895} }
\titlegraphic{\hfill
    \includegraphics[height=20pt]{Caltech-logo}
}
\date{\vspace{3em}  QIT 2019 Tensor Network week -- ICMAT Madrid}

\begin{document}

\maketitle

\section{Tensor networks and quantum phases}

\begin{frame}{Tensor network states}

  \begin{itemize}
    \item Tensor network states (\alert{TNS}) have played a central role in the development of the
      connections between Quantum Information Theory and the Theory of Quantum Many Body Systems.
    \item They provide an efficient description of various classes of states in terms of their
      entanglement structure.
    \item They are useful both for numerical purposes (algorithms) as well as a theoretical tool.
    \end{itemize}

    \begin{exampleblock}{Classification of 1D phases}
      Matrix Product States (MPS) have been crucial to the task of classifying phases and symmetry
      protected topological phases (SPT) in 1D.
    \end{exampleblock}

\end{frame}


\begin{frame}{PEPS}

  \begin{exampleblock}{PEPS}
      I will consider the class of 2D tensor network states known as Projected Entangled Pair States (or \alert{PEPS}).
    \end{exampleblock}

  PEPS are an interesting class of states:
  \begin{itemize}
  \item Efficient representation leads to good numerical algorithms
  \item Good approximation to ground states of many body Hamiltonians
    (Hastings 2006, Molnar et al. 2014)
  \item They can be naturally equipped with a \alert{local Hamiltonian} of which they are ground
    states (Perez-Garcia et al. 2007, Schuch et al. 2010)
  \item They are good candidates for the classification of 2D phases.
  \end{itemize}

\end{frame}

\begin{frame}{Quantum many body systems}

  \begin{columns}[c]

    \column{0.25\textwidth}

    \begin{tikzpicture}
      \node[circle, minimum size=1cm, draw=blue!60, fill=blue!20 ] at (0.75,0.75) {$h$};
      \draw[step=.5cm,densely dotted] (-1.4,-1.4) grid (1.4,1.4);
      \foreach \x in {-1,-0.5,...,1}
      \foreach \y in {-1,-0.5,...,1} {
        \draw (\x, \y) node[spin] {};
        \draw (\y, \x) node[spin] {};
      }
    \end{tikzpicture}

    \column{0.75\textwidth}

    \begin{itemize}
      \item Ingredients:
        \begin{enumerate}
        \item a local (f.d.) Hilbert space $\hs_d = \field C^{d}$
        \item a finite range interaction $h$
        \end{enumerate}
      \item Recipe:
        \begin{enumerate}
        \item $\Lambda$ a finite subset of an (infinite) graph $(G,E)$.
        \item Hilbert space of joint system is $\hs_{\Lambda} = \bigotimes_{u\in \Lambda} \hs_d $
        \item Hamiltonian on $\Lambda$ is given by $H_\Lambda = \sum_{i} h_i \otimes
          \id_{\text{rest}}$
        \end{enumerate}
      \end{itemize}
  \end{columns}

  \pause
      \begin{definition}[spectral gap]
    The \alert{spectral gap} $\lambda(\Lambda)$ of $H_\Lambda$ is the difference between the two smallest eigenvalues
    (without multiplicities).

    We say that $\{H_\Lambda\}_{\Lambda}$ is \alert{gapped} if
     \[ \liminf_{\Lambda \nearrow G} \lambda(\Lambda) >
    0 \]
  \end{definition}

\end{frame}

\begin{frame}{A formal definition of phases}

  \begin{exampleblock}{Phases}

    Two interactions $h^{[0]}$ and $h^{[1]}$ define the same phase if there is a smooth path of
    interactions $[0,1] \ni \alpha \to h^{[\alpha]}$ such that $\inf_{\alpha} \lambda^{[\alpha]} > 0$.
  \end{exampleblock}

  In order to classify phases, we need to understand the spectral gap!

  \pause
  The spectral gap is a very hard quantity to analyze:
  \begin{enumerate}[<+->]
  \item For general 2D translation invariant systems, it is an undecidable problem
    (Cubitt, Perez-Garcia, Wolf 2015)
  \item Gapless: Lieb-Schultz-Mattis theorem (1961) and higher dimensional generalizations (Hastings
    2004, Nachtergaele, Sims 2007)
  \item Positive tools: ``local gap thershold'' (Knabe 1988;
    Gosset, Mozgunov 2016; Lemm 2019), ``martingale method'' (Nachtergaele 1996)
  \end{enumerate}

\end{frame}


\section[PEPS basics]{Projected Entangled Pair States basics}

\begin{frame}{Projected Entangled Pairs States}

  Fix $D\in \field{N}$ (the \emph{bond dimension})

  \begin{columns}[T]
    \column{0.50\textwidth}
    \begin{center}
      \begin{overlayarea}{\textwidth}{1cm}
    \begin{tikzpicture}[scale=1.8]
      \draw[step=0.6cm,densely dotted] (-1.4,-1.4) grid (1.4,1.4);
      \draw<5->[-,blue!50, thick] (-1.2,1.2) -- (1.2,1.2) -- (1.2,-1.2) --
      node[midway, label={[blue]below:$\ket{X}$}] {}
       (-1.2,-1.2) --  cycle;
       \draw[dashed, orange] (1,1) -- (1,-1) -- (-1,-1) -- (-1,1)
       node[midway, label={above right:$\Lambda$}] {}
        --  cycle;
      \foreach \y in {-0.6, 0, 0.6 }
        \foreach \x in {-1.2, -0.6, 0, 0, 0.6}{
          \draw<2->[epr] (\x,\y) ++(0.1,0) -- +(0.4, 0);
          \draw<2->[epr] (\y,\x) ++(0,0.1) -- +(0, 0.4);
        }
      \foreach \y in {-0.6, 0, 0.6 }
       \foreach \x in {-0.6, 0, 0, 0.6}{
          \node<3->[tensor,scale=0.8] (t\x\y) at (\x,\y) {$T$};
        }
        \foreach \y in {-0.6, 0, 0.6}
        \foreach \x in {-1.2, 1.2}{
          \node<4->[spin] (s\x\y) at (\x, \y) {};
          \node<4->[spin] (s\y\x) at (\y, \x) {};
        }
      \end{tikzpicture}
    \end{overlayarea}
  \end{center}

  \column{0.50\textwidth}
    \pause
    \begin{itemize}[<+->]
    \item At each $e\in E_{\bar \Lambda}$ put $\ket{\omega_e} = D^{-1/2} \sum_{j=1}^D \ket{j,j}$
    \item For each $v\in \Lambda$, consider $T_v: \hs_D^{\otimes \deg(v)} \to \hs_d$ linear
    \item For $\Lambda\subset G$, denote $\hs_{\partial \Lambda} = \bigotimes_{e \in \partial
        \Lambda} \hs_D$
    \item Then for each $\ket{X} \in \hs_{\partial \Lambda}$ we can define a state in $\hs_{\Lambda}$ by
    \end{itemize}
    \begin{equation*}
      \uncover<5->{\ket{\phi_{\Lambda,X}} = \bra{X}}
      \uncover<3->{\bigotimes_{v\in \Lambda} T_v}
      \uncover<2->{\bigotimes_{e\in E_{\bar \Lambda}} \ket{\omega_e}}
    \end{equation*}
  \end{columns}
  \end{frame}

\section{Spectral gap and injectivity}

  \begin{frame}{Parent Hamiltonian and injectivity}
    \begin{block}{Parent Hamiltonian}
        There exist a local, \alert{frustration-free} Hamiltonian $H_\Lambda = \sum_{p}h_p$, with
        $h_p$ projections, such that $\ket{\phi_{\Lambda,X}}$ is a groundstate for every $\ket{X} \in
        \hs_{\partial \Lambda}$.
      \end{block}
      \begin{exampleblock}{frustration-free}
        \[ h_p \ket{\phi_{\Lambda, X}} = 0 , \quad \forall \ket X \in \hs_{\partial \Lambda}, \forall p
          .\]
      \end{exampleblock}
  \pause
  \begin{definition}[injectivity]
    A PEPS is \alert{injective} on $\Lambda$ if the linear map $V_\Lambda$
    \begin{align*}
    V_\Lambda : & \hs_{\partial \Lambda} \to \hs_{\Lambda} \\
                & \ket{X} \mapsto \ket{\phi_{\Lambda, X}} =
                  \bra{X} \bigotimes_{v\in \Lambda} T_v \bigotimes_{e\in E_{\bar \Lambda}} \ket{\omega_e}
    \end{align*}
    is injective.
  Note that $\ker h_p = \ima V_p$.


  \end{definition}
\end{frame}

\begin{frame}{The 1D case: Matrix Product States}

  In 1D, the boundary Hilbert space can be identified with the algebra of $D\times D$ matrices:
    \[
    \hs_{\partial \Lambda} = \field{C}^D\otimes \field{C}^D \simeq \matrixalg_D
  \]
  so that the linear map $V^*_{[1,n]}$ can be expanded in the canonical basis as a list of matrices:
  \[
    V_{[1,n]}^* \simeq \sum_{i_1,\dots,1_n=1}^d M_{i_1,\dots,i_n} \bra{i_1,\dots,i_n}.
  \]
  It is easy to see that the tensor construction implies that
  \[ M_{i_1,\dots,i_n} = M_{i_1} \cdots M_{i_n}  \]
  which is the more usual way Matrix Product States are defined.
\end{frame}

\begin{frame}{Injectivity in 1D}
  Up to renormalization, we can assume w.l.o.g. that $T = V_{[1]}$ is injective.

  \begin{block}{Proposition}
    The following are equivalent
    \begin{enumerate}
    \item $T$ is injective;
    \item $\linspan \{ M_i \} = \matrixalg_D$;
    \item the \alert{transfer operator} given by $\traop(X) = \sum_{i} M_i X M_i^*$ is primitive.
    \end{enumerate}
  \end{block}
\pause
  Primitivity of the tranfer operator implies:
  \begin{itemize}
    \item $\traop$ has a dominant simple real eigenvalue
    \item ... whose eigenvector is a full rank positive operator
    \item all other eigenvalues have real part strictly smaller
    \item which implies $\traop^n$ converges (up to normalization) to a rank-one projection exponentially fast.
   \end{itemize}

   The \alert{spectral gap $\mu$} of $\traop$ controls the exponential decay of two-point
   correlations in the chain.

\end{frame}

\begin{frame}{1D spectral gap}
  Surprisingly (for some at least), primitivity of the transfer operator directly implies that the parent Hamiltonian is gapped.

  \begin{theorem}[{\small Fannes, Nachtergaele, Werner 1992; Perez-Garcia, Verstraete, Wolf, Cirac 2007}]
    If a MPS is injective (eventually after blocking), then its parent Hamiltonian has a spectral gap independent of
    $\Lambda$.
  \end{theorem}
\pause
  \begin{block}{Remarks}
    \begin{itemize}
    \item The spectral gap $\mu$ of $\traop$ controls both decay of correlations and spectral gap of
      the parent Hamiltonian.
    \item Primitivity is a ``local'' property: it can be checked by computing the spectrum of
      $\traop$ (depending on tensors on a finite size region).
    \end{itemize}
  \end{block}

  This allows to deform the tensors and move in the space of MPS while making sure that the spectral gap will not close.

\end{frame}

\begin{frame}{Injectivity in 2D}
  \begin{alertblock}{In 2D injectivity is not enough!}
    \vspace{0.5em}
    In 2D, there are injective PEPS with long-range correlations, which \alert{cannot} be
    groundstates of local, gapped Hamiltonians.
  \end{alertblock}
\pause
\begin{exampleblock}{Ising PEPS}
  \vspace{0.5em}
    The Ising PEPS is a purification of the Gibbs state of a classical Ising model at finite
    temperature. It is an injective PEPS (on some geometries).

    It also reproduces the same two-point correlation function of the original Ising model.

    Therefore, at the critical temperature, this PEPS cannot be groundstate of a gapped Hamiltonian.
  \end{exampleblock}

\end{frame}

\section{Boundary states}

\begin{frame}{Boundary states of PEPS}

  \begin{definition}[Boundary state]
    We denote
    $
      \rho_{\partial A} = V_A^{*}V_A^{\phantom{*}} \in \mcl B(\hs_{\partial A})
   $ the boundary state of $A$.
  \end{definition}

  \begin{figure}
    \includegraphics[width=0.50\textwidth]{BndA.pdf}
  \end{figure}

  \begin{itemize}
  \item $\rho_{\partial A} \ge 0$ (unormalized density operator)
  \item $\ker \rho_{\partial A} = \ker V_A$ (so $\rho_{\partial A} > 0$ is the PEPS is injective)
  \item for MPS, the boundary state is the Choi-Jamio\l{}kowski state of the (iterated) transfer operator
  \end{itemize}
\end{frame}

\begin{frame}{Properties of the boundary state}
  $ P_A =   V_A \rho_{\partial A}^{-1} V_A^* =
  \begin{tikzpicture}[baseline={([yshift=-.5ex]current bounding box.center)}, scale=0.8]
      \foreach \i in {1,...,2} {
      \node[tensor] (A\i) at (\i, 1.5) {$T$};
      \node[tensor] (B\i) at (\i, 0)  {$T$};
      \draw[-] (A\i) -- +(0,+0.5);
      \draw[-] (B\i) -- +(0,-0.5);
    };

    \draw[boundary] (0.5,0.5) rectangle +(2,0.5) node[midway] (r) {$\rho_{\partial A}^{-1}$};

    \foreach \i in {1,...,1} {
        \pgfmathtruncatemacro{\iplusone}{\i + 1};
        \draw[-] (A\i) -- (A\iplusone);
        \draw[-] (B\i) -- (B\iplusone);
      };

      \draw[-] (A1.west) to[out=180,in=180,distance=0.5cm] (0.5,1);
      \draw[-] (B1.west) to[out=180,in=180,distance=0.5cm] (0.5,0.5);
      \draw[-] (A2.east) to[out=0,in=0,distance=0.5cm] (2.5,1);
      \draw[-] (B2.east) to[out=0,in=0,distance=0.5cm] (2.5,0.5);

    \end{tikzpicture}
    $ is a projection on $\ima V_A$.
    \begin{overlayarea}{\textwidth}{4cm}
      \[
   P_A \ket{\phi_{\Lambda,X}} =
     \begin{tikzpicture}[baseline={([yshift=-.5ex]current bounding box.center)}, scale=0.8]
      \foreach \i in {1,...,2} {
      \node<-3>[tensor] (A\i) at (\i, 1.5) {$T$};
      \node<1>[tensor] (B\i) at (\i, 0)  {$T$};
      \node<1,4>[tensor] (C\i) at (\i, -1)  {$T$};
      \draw<-3>[-] (A\i) -- +(0,+0.5);
      \draw<1>[-] (B\i) -- +(0,-0.5);
      \draw<1,4>[-] (C\i) -- +(0, 0.5);;
    };

    \foreach \i in {3,...,5} {
      \node[tensor] (C\i) at (\i, -1)  {$T$};
      \draw[-] (C\i) -- +(0,0.5);
    }

    \draw<-2>[boundary] (0.5,0.5) rectangle +(2,0.5) node[midway] (r) {$\rho_{\partial A}^{-1}$};
    \draw[boundary] (1,-2) rectangle +(4,0.5) node[midway] (X) {$X$};

    \foreach \i in {1,...,1} {
        \pgfmathtruncatemacro{\iplusone}{\i + 1};
        \draw<-3>[-] (A\i) -- (A\iplusone);
        \draw<1>[-] (B\i) -- (B\iplusone);
        \draw<1,4>[-] (C\i) -- (C\iplusone);
      };

      \draw<-2,4>[-] (C2) -- (C3);
      \foreach \i in {3,...,4} {
        \pgfmathtruncatemacro{\iplusone}{\i + 1};
        \draw[-] (C\i) -- (C\iplusone);
      };

      \draw<2>[boundary] (0.7,-1.25) rectangle +(1.6,1.5) node[midway] {$\rho_{\partial A}$};

      \draw<-2>[-] (A1.west) to[out=180,in=180,distance=0.5cm] (0.5,1);
      \draw<-2>[-] (A2.east) to[out=0,in=0,distance=0.5cm] (2.5,1);
      \draw<-2>[-] (B1.west) to[out=180,in=180,distance=0.5cm] (0.5,0.5);
      \draw<-2>[-] (B2.east) to[out=0,in=0,distance=0.5cm] (2.5,0.5);
      \draw<-2,4>[-] (C1.west) to[out=180,in=180,distance=0.5cm] (1,-1.75);
      \draw[-] (C5.east) to[out=0,in=0,distance=0.5cm] (5,-1.75);

      \draw<3>[-] (A1.west) to[out=180,in=180,distance=0.5cm] (1,-1.75);
      \draw<3>[-] (A2.east) to[out=0,in=180,distance=0.5cm] (C3.west);


    \end{tikzpicture}
    \onslide<4>{
      = \ket{\phi_{\Lambda,X}}
    }
  \]
\end{overlayarea}
\end{frame}

\begin{frame}{Boundary state and entanglement spectrum}
  \begin{block}{Entanglement spectrum}
    The \alert{entanglement spectrum} is the spectrum of the reduced density matrix of a state.
  \end{block}
  \begin{overlayarea}{\textwidth}{3cm}
  \[
    \onslide<1>{\dyad{\phi_\Lambda}}
    \onslide<2->{\phi_A}
    =
    \begin{tikzpicture}[baseline={([yshift=-.5ex]current bounding box.center)}, scale=0.7]

      \foreach \i in {3,4} {
        \node[tensor] (A\i) at (\i, 2.5) {$T$};
        \node[tensor] (B\i) at (\i, 0)  {$T$};
      };

      \foreach \i in {1,2,5,6} {
        \node<-2>[tensor] (A\i) at (\i, 2.5) {$T$};
        \node<-2>[tensor] (B\i) at (\i, 0)  {$T$};
      };

      \foreach \i in {3,4} {
        \draw[-] (A\i) -- +(0,0.5);
        \draw[-] (B\i) -- +(0,-0.5);
      }

      \foreach \i in {1,2,5,6} {
        \draw<1>[-] (A\i) -- +(0,+0.5);
        \draw<1>[-] (B\i) -- +(0,-0.5);
        \draw<2>[-] (A\i) -- (B\i);
      };


      \foreach \i in {1,...,5} {
        \pgfmathtruncatemacro{\iplusone}{\i + 1};
        \draw<-2>[-] (A\i) -- (A\iplusone);
        \draw<-2>[-] (B\i) -- (B\iplusone);
      };

      \draw<-2>[-] (A1.west) to[out=180,in=180,distance=0.5cm] (1, 3.5) -- (6,3.5)
      to[out=0,in=0,distance=0.5cm] (A6.east);
      \draw<-2>[-] (B1.west) to[out=180,in=180,distance=0.5cm] (1, -1) -- (6,-1)
      to[out=0,in=0,distance=0.5cm] (B6.east);

      \draw<3>[boundary] (2.5,1) rectangle +(2,0.5) node[midway] (r) {$\rho_{\partial A^c}$};
      \draw<3>[-] (A3) -- (A4);
      \draw<3>[-] (B3) -- (B4);
      \draw<3>[-] (A3.west) to[out=180,in=180,distance=0.5cm] (2.5,1.5);
      \draw<3>[-] (A4.east) to[out=0,in=0,distance=0.5cm] (4.5,1.5);
      \draw<3>[-] (B3.west) to[out=180,in=180,distance=0.5cm] (2.5,1);
      \draw<3>[-] (B4.east) to[out=0,in=0,distance=0.5cm] (4.5,1);

    \end{tikzpicture}
  \onslide<3>{ = V_A \rho_{\partial A^c}V_A^* \sim \rho_{\partial A}^{1/2}\rho_{\partial
    A^c}\rho_{\partial A}^{1/2} }\]

\onslide<3>{In some particular cases (half-cilinders with symmetry) $\rho_{\partial A} =
  \rho_{\partial A^c}$, so the spectrum of $\phi_A$ is simply the spectrum of $\rho_{\partial A}^2$.}
\end{overlayarea}
\end{frame}

\begin{frame}{Entanglement spectrum and boundary Hamiltonian}
  \begin{exampleblock}{Li, Haldane 2008}
    The entanglement spectrum of a fractional quantum Hall ground state resembles a conformal
    field theory at finite temperature at the boundary.

    \emph{``Entanglement entropy is equivalent to the thermodynamic entropy''}
  \end{exampleblock}

  \pause

  PEPS allow to make this connection rigorous:
  \begin{definition}[Boundary Hamiltonian]
    \[ H_{\partial A} = \log \rho_{\partial A} \qq{or} \quad \rho_{\partial A} = e^{H_{\partial A}}\]
  \end{definition}

  \begin{exampleblock}{Conjecture (numerical evidence)}
    The parent Hamiltonian of the PEPS is gapped if and only if the boundary
    Hamiltonian is short range.
  \end{exampleblock}

\end{frame}

\section{Boundary states and spectral gap}

\begin{frame}{The spectral gap for 2D PEPS}

  \begin{theorem}[1]
    If the boundary state of a PEPS is \alert{approximately factorizable}, then the parent
    Hamiltonian is gapped.
  \end{theorem}

  \begin{exampleblock}{approximately factorizable}
    A 1D state is approximately factorizable if $\rho_{ABC} \simeq \Delta_{AB} \Omega_{BC}$.
  \end{exampleblock}
  \pause
  For MPS, injectivity implies that the 0D boundary state is approximately factorizable.
  \pause
  \begin{theorem}[2]
    Gibbs states of \emph{finite range} 1D local Hamiltonians are approximately factorizable.
  \end{theorem}
  \pause
  We can treat the G-injective and MPO-injective case as well, but I will mostly focus on injective
  PEPS for simplicity.
\end{frame}

\section{Sketch of the proof: theorem (1)}

\begin{frame}{A generalized martingale condition}

  In order to prove the spectral gap, we use an equivalence based on a generalization of the
  ``martingale method'':
  \begin{lemma}[Kastoryano, L., arxiv:1705.09491]
    A frustration-free Hamiltonian is gapped if and only if it has almost commuting groundstate projections:
    \[ \delta(AB,BC) := \norm{P_{AB}P_{BC} - P_{ABC}} \le \alpha^{\ell_B},\quad \alpha < 1 \]
  \begin{center}
    \includegraphics[width=0.55\textwidth]{fig3}
  \end{center}
  \end{lemma}

  This is based on a recursive argument: the gap of $ABC$ can be lower bounded by the gaps of $AB$
  and $BC$ up to a constant depending on $\delta(AB,BC)$.
\end{frame}

\begin{frame}{Almost commuting groundstate projections}
 $P_A = V_A \rho_{\partial A}^{-1} V_A^*$, so that $P_{AB}P_{BC}$ is equal to

  \begin{figure}
    \begin{tikzpicture}
      \foreach \i in {1,...,5} {
      \node[tensor] (A\i) at (\i, 1.5) {$T$};
      \node[tensor] (B\i) at (\i, 0)  {$T$};
      \draw  (A\i) +(2,-3) node[tensor] (C\i) {$T$};
      \draw  (B\i) +(2,-3) node[tensor] (D\i) {$T$};
      \draw[-] (A\i) -- +(0,+0.5);
      \draw[-] (B\i) -- +(0,-0.5);
      \draw[-] (C\i) -- +(0,+0.5);
      \draw[-] (D\i) -- +(0,-0.5);
    };

    \draw[boundary] (1,0.5) rectangle +(4,0.5) node[midway] (r) {$\rho_{\partial AB}^{-1}$};
    \draw[boundary] (3,-2.5) rectangle +(4,0.5) node[midway] (s) {$\rho_{\partial BC}^{-1}$};

    \foreach \i in {1,...,4} {
        \pgfmathtruncatemacro{\iplusone}{\i + 1};
        \draw[-] (A\i) -- (A\iplusone);
        \draw[-] (B\i) -- (B\iplusone);
        \draw[-] (C\i) -- (C\iplusone);
        \draw[-] (D\i) -- (D\iplusone);
      };

      \draw[-] (B3) -- (C1);
      \draw[-] (B4) -- (C2);
      \draw[-] (B5) -- (C3);

      \draw[-] (A1.west) to[out=180,in=180,distance=0.5cm] (1,1);
      \draw[-] (B1.west) to[out=180,in=180,distance=0.5cm] (1,0.5);
      \draw[-] (A5.east) to[out=0,in=0,distance=0.5cm] (5,1);
      \draw[-] (B5.east) to[out=0,in=0,distance=0.5cm] (5,0.5);

      \draw[-] (C1.west) to[out=180,in=180,distance=0.5cm] (3,-2);
      \draw[-] (D1.west) to[out=180,in=180,distance=0.5cm] (3,-2.5);
      \draw[-] (C5.east) to[out=0,in=0,distance=0.5cm] (7,-2);
      \draw[-] (D5.east) to[out=0,in=0,distance=0.5cm] (7,-2.5);

      \only<2>{
        \draw[boundary] (2.7, 0.3) rectangle (5.3, -1.8) node[midway] (t) {$\rho_{\partial B}$};
      }

    \end{tikzpicture}
  \end{figure}

\end{frame}

\begin{frame}{Building intuition: product boundary states}
 If the boundary is product and independent of the system size, then:

  \begin{figure}
    \begin{tikzpicture}
      \foreach \i in {1,...,5} {
      \node[tensor] (A\i) at (\i, 1.5) {$T$};
      \draw  (\i,0) +(2,-3) node[tensor] (D\i) {$T$};
      \draw[-] (A\i) -- +(0,+0.5);
      \draw[-] (D\i) -- +(0,-0.5);
    };

    \foreach \i in {1,...,2}{
      \node[tensor] (B\i) at (\i, 0)  {$T$};
      \draw  (\i, 1.5) +(5,-3) node[tensor] (C\i) {$T$};
      \draw[-] (B\i) -- +(0,-0.5);
      \draw[-] (C\i) -- +(0,+0.5);
    }

    \foreach \i in {1,...,4} {
        \pgfmathtruncatemacro{\iplusone}{\i + 1};
        \draw[-] (A\i) -- (A\iplusone);
        \draw[-] (D\i) -- (D\iplusone);
      };

      \draw[-] (B1) -- (B2);
      \draw[-] (C1) -- (C2);

      \node[boundary] (r1) at (1,0.7) {$\rho_{L}^{-1}$};
      \draw[-] (A1.west) to[out=180, in=180] (r1.north west);
      \draw[-] (B1.west) to[out=180, in=180] (r1.south west);
      \node[boundary] (s2) at (7,-2.2) {$\rho_{R}^{-1}$};
      \draw[-] (C5.east) to[out=0, in=0] (s2.north east);
      \draw[-] (D5.east) to[out=0, in=0] (s2.south east);

      \only<1>{
      \node[boundary] (r2) at (5,0.7) {$\rho_{R}^{-1}$};
      \node[boundary] (s1) at (3,-2.2) {$\rho_{L}^{-1}$};

      \node[boundary] (t1) at (2.7,-0.7) {$\rho_{L}$};
      \node[boundary] (t2) at (5.3,-0.7) {$\rho_{R}$};

      \draw[-] (A5.east) to[out=0, in=0] (r2.north east);

      \draw[-] (D1.west) to[out=180, in=180] (s1.south west);

      \draw[-] (r2.south east) to[out=0,in=90] (t2.north);
      \draw[-] (t2.south) to[out=-90, in=180] (C1.west);

      \draw[-] (s1.north west) to[out=180,in=-90] (t1.south);
      \draw[-] (t1.north) to[out=90, in=0] (B2.east);
    }
    \only<2>{
      \draw[-] (B2.east) to[out=0,in=180] (D1.west);
      \draw[-] (A5.east) to[out=0,in=180] (C1.west);
    }

    \end{tikzpicture}
  \end{figure}

\end{frame}
\begin{frame}{Building intuition: product boundary states}
 If the boundary is product, and independent of the system size.

  \begin{figure}
    \begin{tikzpicture}
      \foreach \i in {1,...,7} {
      \node[tensor] (A\i) at (\i, 1.5) {$T$};
      \node[tensor] (B\i) at (\i, 0)  {$T$};
      \draw[-] (A\i) -- +(0,+0.5);
      \draw[-] (B\i) -- +(0,-0.5);
    };

    \foreach \i in {1,...,6} {
        \pgfmathtruncatemacro{\iplusone}{\i + 1};
        \draw[-] (A\i) -- (A\iplusone);
        \draw[-] (B\i) -- (B\iplusone);
      };

      \node[boundary] (r1) at (1,0.7) {$\rho_{L}^{-1}$};
      \node[boundary] (r2) at (7,0.7) {$\rho_{R}^{-1}$};
      \draw[-] (A1.west) to[out=180, in=180] (r1.north west);
      \draw[-] (B1.west) to[out=180, in=180] (r1.south west);

      \draw[-] (A7.east) to[out=0, in=0] (r2.north east);
      \draw[-] (B7.east) to[out=0, in=0] (r2.south east);
    \end{tikzpicture}
  \end{figure}

  Then we have that $P_{AB}P_{CB} = P_{ABC}$ exactly.

  \begin{block}{Isometric case}
    This happens if $V_A$ is an \alert{isometry}. In this case the parent Hamiltonian is commuting
    and therefore trivially gapped.
  \end{block}

\end{frame}

\begin{frame}{Generalizing to non-product states}
We would like to make the previous argument work ``up to $\epsilon$''. But what is the correct
``measure'' of ``close to a product'' in this case?

\alert{Idea:} replace $\rho_{\partial X}^{-1}$ by some $\sigma_{\partial X}^{-1}$ and let $\tilde
P_X = V_X \sigma_X^{-1} V_X^*$ such that
\begin{enumerate}
\item $P_X$ is close to $\tilde P_X$;
\item $\tilde P_X$ are almost commuting.
\end{enumerate}


\pause
\begin{block}{Proposition}
  Let $\sigma_{\partial X}$ be an invertible operator on $\hs_{\partial X}$, and $\tilde P_X = V_X \sigma_X^{-1} V_X^*$. Then
  \begin{align*}
    \norm{\tilde P_X} &= \norm{\rho_{\partial X}^{1/2}\sigma_{\partial X}^{-1} \rho_{\partial
                        X}^{1/2}}; \\
    \norm{\tilde P_X - P_X} &= \norm{\rho_{\partial X}^{1/2}\sigma_{\partial X}^{-1} \rho_{\partial
                              X}^{1/2} - \id} .
  \end{align*}
\end{block}

\end{frame}

\begin{frame}{Making \texorpdfstring{$\tilde P_X$}{tilde P_X} almost commuting}

  \begin{figure}
    \includegraphics[width=0.6\textwidth]{fig4.pdf}
  \end{figure}

  \begin{block}{Lemma}
    For the partition above, let $\Delta_{zb}, \Delta_{az}, \Omega_{zc}, \Omega_{dz}$ invertible
    operators (whose support is given by the subscript), and denote by
    \begin{align*}
      \sigma_{\partial ABC} &=\Delta_{zb} \Delta_{az} & \sigma_{\partial B} &= \Omega_{zc}\Omega_{dz}
      \\
      \sigma_{\partial AB} &=\Omega_{zb} \Delta_{az} & \sigma_{\partial BC} &= \Delta_{zc}\Omega_{dz}
    \end{align*}
    \vspace{-1em}
    Then
    $
    \norm{\tilde P_{AB} \tilde P_{BC} - \tilde P_{ABC}} \le\norm{\tilde P_{AB}} \norm{\tilde P_{BC}}
    \norm{\rho_{\partial B}^{-1/2} \sigma_{\partial B} \rho_{\partial B}^{-1/2} - \id}.
    $

  \end{block}
\end{frame}

\begin{frame}{Approximate factorization}

  \begin{block}{Definition}
    If we can find a an approximation to $\rho_{\partial R}$ as above for $R \in \{ABC,AB,BC,B\}$,
    such that
    \begin{itemize}
    \item $\tilde P_R$ is close to $P_R$: $\norm{\rho^{1/2}_{\partial R} \sigma_{\partial
          R}^{-1}\rho^{1/2}_{\partial R} - \id} \le \epsilon$ ($R \in \{ABC, AB, BC\}$);
    \item $\tilde P_B$ are almost commuting: $\norm{\rho_{\partial B}^{-1/2}\sigma_{\partial B}
        \rho^{-1/2}_{\partial B} -\id} \le \epsilon$,
    \end{itemize}
    then we say that the boundary is $\epsilon$-approximately factorizable with respect to $ABC$.
  \end{block}

  \begin{block}{Theorem}
    If this condition holds, then
    \[ \norm{P_{AB}P_{BC} - P_{ABC}} \le 8\epsilon\]
  \end{block}
\end{frame}

\begin{frame}{Reviewing MPS injectivity}

  In 1D, MPS injectivity is strong enough to guarantee approximate factorization.

  \pause
  \[
    \norm{\traop^{n} - \traop^{\infty}} \le c \mu^n
  \]
  implies that
  \[
    \norm{\rho_{\partial[1,n]} - \sigma \otimes \id}_1 \le c\mu^n
  \]

  and since the dimension of $\hs_{\partial[1,n]}$ does not increase with $n$ we can use that
  \[
    \norm{X^{1/2}Y^{-1}X^{1/2} - \id} \le Y_{\min}^{-1} \norm{X-Y}
  \]
  holds for every positive invertible operators $X$ and $Y$.

\end{frame}

\section{Sketch of the proof: theorem (2)}
\begin{frame}{Sketch of Theorem (2)}
  Theorem (2) proof is based on Arakis' theorem: evolution semigroup generated by a 1D spin chain
  with finite range interactions is entire analytic.

  \alert{Idea:}
  imaginary time evolution $\rho_{\partial \Lambda} = e^{H_{\partial \Lambda}}$
  $\to$
  real time evolution $e^{i H_{\partial \Lambda}}$
  $\to$
  finite unitary depth circuit.

  \vspace{1em}

  \begin{overlayarea}{\textwidth}{3cm}
    \begin{center}
      \begin{tikzpicture}

        \tikzset{gate/.style={rectangle, thick, minimum width=0.8cm,
            minimum height=0.3cm},
          gateA/.style={draw=blue!50,fill=blue!20},
          gateB/.style={draw=magenta!50, fill=magenta!20}
        }

        \foreach \i in {1,...,4}{
          \node[gate,gateA] (A\i) at (\i, 0.5) {};
        }
        \foreach \i in {5,...,8}{
          \node<1>[gate,gateA] (A\i) at (\i, 0.5) {};
          \node<2>[gate,gateB] (A\i) at (\i, 0.5) {};
        }
        \foreach \i in {1,...,3}{
          \node[gate, gateA] (B\i) at (\i.5, 0)  {};
          \node[gate,gateA] (C\i) at (\i, -0.5) {};
        }
        \foreach \i in {4,...,7}{
          \node<1>[gate, gateA] (B\i) at (\i.5, 0)  {};
          \node<2>[gate, gateB] (B\i) at (\i.5, 0)  {};
        }
        \foreach \i in {4,...,8}{
          \node<1>[gate,gateA] (C\i) at (\i, -0.5) {};
          \node<2>[gate,gateB] (C\i) at (\i, -0.5) {};
        }

        \draw<2>[-,thick] (0.5,-0.75) -- +(2.8,0) node[midway,below] {$A$};
        \draw<2>[-,thick] (3.5,-0.75) -- +(0.9,0) node[midway,below] {$B$};
        \draw<2>[-,thick] (4.6,-0.75) -- +(3.9,0) node[midway,below] {$C$};

      \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \onslide<2>{
      If $\rho_{ABC}$ were generated by a finite depth circuit, then it would hold exactly that $ \rho_{ABC} = \Delta_{AB} \Omega_{BC}$.
      For real time evolution, a similar argument can be made using Lieb-Robinson bounds.
      For imaginary time evolution, we rely instead on Araki's theorem.
    }
  \end{overlayarea}


\end{frame}

\begin{frame}{Splitting the boundary}

  \begin{figure}
    \includegraphics[width=0.6\textwidth]{bndfig2.pdf}
  \end{figure}
  \begin{align*}
    \sigma_{\partial ABC} &= \Delta_{xyb}\Delta_{axy} & \sigma_{\partial B} &=
                                                                              \Omega_{xyc}\Omega_{dxy},
    \\
    \sigma_{\partial AB} &= \Omega_{xyc}\Delta_{axy} & \sigma_{\partial BC} &= \Delta_{xyb}\Omega_{dxy};
  \end{align*}
  and
  \begin{align*}
    \Delta_{axy} &= e^{H_{ax}} e^{-H_y}e^{H_{axy}} ;\\
    \Omega_{xyc} &= e^{H_{xyc}} e^{-H_x}e^{H_{yc}} ;
  \end{align*}
  and the same for $b$, $d$.

\end{frame}

\begin{frame}{Expansionals}

  With this choice of $\Delta_*$ and $\Omega_*$, the conditions we need to verify for approximate
  factorization are norms bounds on products of exponentials of non-commuting local Hamiltonians.

  \begin{block}{Expansionals (Araki)}
    For $O_1, \dots, O_n$ operators holds that
    \[
      e^{O_1} \cdots e^{O_n} = \OExp\qty[ \int_0^1 \dd{t} \sum_{m=1}^n \Gamma_{O_1}^t \circ \cdots
      \circ \Gamma_{O_{m-1}}^t (O_m) ]
    \]
    where $\Gamma_{X}^t(Y) = e^{tX} Y e^{-tX}$ and $\OExp$ is the expansional / time-ordered exponential.
  \end{block}

  So in order to verify the approximate factorization condition we need to bound compositions of
  imaginary time evolutions of local Hamiltonians.

\end{frame}

\begin{frame}{Imaginary time evolution lightcones}

  We can do so using Araki's theorem.

  \begin{block}{Theorem (Araki 1969)}
    \vspace{0.5em}
    Let $Q_\Sigma=\sum_{Z\subseteq \Sigma} q_Z$ be a local Hamiltonian on $\Sigma$ with
    interaction length $r$ and strength $J$, and let $f$ be an observable with support on
    $[-n;n]$. Then
    \vspace{-1em}
    \begin{align*}
      \norm{\Gamma^t_\Sigma(f)-\Gamma^t_\ell(f)} &\leq \chi_{\ell}(\tau)F_n(\tau)\norm{f} \\
      \norm{\Gamma^t_\Sigma(f)}&\leq F_n(\tau) \norm{f}
    \end{align*}
    where $\tau = 2tJ$ and the functions $\chi_\ell$ and $F_n$ can be bounded as
    \[  F_n(x)\leq e^{(n-r+1)x+2\log(r)e^{xr}}, \quad\text{and}\quad
     \chi_\ell(x)\leq\frac{\left[2\log(r)e^{rx}\right]^{\lfloor \ell/r\rfloor+1}}{(\lfloor
        \ell/r\rfloor+1)!} \]

  \end{block}

\end{frame}

\begin{frame}{Proving theorem (2)}
  Putting everything together, we can use Araki's theorem to show that if $\log(\rho_{\partial X}) =
  \sum_{Z \subset X} q_Z$ is a finite-range Hamiltonian for every rectangular region $X$, then we
  have
  \begin{block}{Proposition}
    \[ \norm{\rho_{\partial AB}^{1/2} \sigma_{\partial AB}^{-1} \rho_{\partial AB}^{1/2} - \id} \le
      \epsilon(\ell_B) \]
    where the function $\epsilon(\ell_B)$ depends on interaction strength and range and Araki's
    functions.
  \end{block}
  The same holds for the other regions $ABC$, $BC$, $B$, so Theorem (2) follows.
\end{frame}

  \section{Conclusions and outlook}

  \begin{frame}{Conclusions and outlook}

    \begin{exampleblock}{Main results combined}
      For 2D PEPS, finite interaction range in the boundary Hamiltonian implies a spectral gap for the
      bulk parent Hamiltonian.
    \end{exampleblock}
    \pause
    \begin{exampleblock}{Open problems}
      \begin{enumerate}[<+->]
      \item  We lack any interesting example where a proof of the spectral gap was not already known
        via other means.
        \begin{itemize}[<+->]
        \item Does Theorem (2) hold for exponentially decaying interactions?
        \item Antonio Perez, David Perez-Garcia, work in progress:  Araki's theorem does generalize to
        exponentially decaying interaction \alert{with some restriction on the decay rate}.
      \item Can we get confirm the numerical results about the boundary Hamiltonian of interesting
      models analytically?
      \end{itemize}

      \item Are there cases were the condition implying the gap can be checked ``locally'', so that
        one can deform the tensor without closing the gap?
    \item Does temperature on the boundary play a role? How to define it?
    \end{enumerate}
  \end{exampleblock}
  \uncover<+->{
    \vspace{-1em}
  \begin{center}
    {\Large Thank you!}
  \end{center}
  }
\end{frame}

\end{document}

%%% Local Variables:
%%% coding: utf-8
%%% mode: latex
%%% TeX-engine: xetex
%%% TeX-master: t
%%% End:
