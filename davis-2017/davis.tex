\documentclass[10pt,compress]{beamer}

\usepackage{appendixnumberbeamer}
\usetheme[light]{tutorial}

\usepackage{physics}

\usetikzlibrary{decorations.pathmorphing}
\tikzset{
  onslide/.code args={<#1>#2}{ \only<#1>{\pgfkeysalso{#2}} },
  tensor/.style={rectangle,draw=blue!50,fill=blue!20, thick},
  spin/.style={circle,draw=blue!60, fill=blue!50, scale=0.4},
  epr/.style={draw=magenta!70,-, decorate, decoration={snake, amplitude=0.05cm, segment
      length=0.1cm}},
  boundary/.style={draw=yellow!70,fill=yellow!40, thick}
}

\usepackage{xcolor}
\definecolor{LightGray}{rgb}{0.45,0.5,0.45}

% math fonts
\usepackage{dsfont}
\usepackage{bm}
\newcommand{\mcl}{\mathcal}

% fields and rings
\newcommand{\field}[1]{\ensuremath{\mathds{#1}}}
\newcommand{\hs}{\mcl H}

\DeclareMathOperator{\ima}{Im}
\DeclareMathOperator{\OExp}{Exp}
\DeclareMathOperator{\supp}{supp}

\AtBeginSection[]{
  \begin{frame}
  \vfill
  \centering
  \begin{beamercolorbox}[sep=8pt,center]{title}
    \usebeamerfont{title}\insertsection\par%
  \end{beamercolorbox}
  \vfill
  \end{frame}
}

\title{Boundaries and spectral gaps of 2D PEPS}
\subtitle{
  \textcolor{LightGray}{Based on \texttt{arxiv:1705.09491} and \texttt{1709.07691}}
}
\author[A. Lucia]{Angelo Lucia (QMATH, University of Copenhagen)}
\titlegraphic{\includegraphics[height=30pt]{figures/qmath-center}}
\date{Joint work wit M.~J.~Kastoryano and D.~Perez-Garcia}
\institute[QMATH]{
  \hfill \includegraphics[height=80pt]{figures/bikes}
  \vspace{-10em}
}

\begin{document}

\maketitle

\begin{frame}{Outline}
  \setbeamertemplate{section in toc}[sections numbered]
  \tableofcontents
\end{frame}

\section[PEPS basics]{Projected Entangled Pair States basics}

\begin{frame}{Tensor network states}

  \begin{columns}[c]

    \column{0.25\textwidth}

    \begin{tikzpicture}
      \draw[step=.5cm,densely dotted] (-1.4,-1.4) grid (1.4,1.4);
      \foreach \x in {-1,-0.5,...,1}
      \foreach \y in {-1,-0.5,...,1} {
        \draw (\x, \y) node[spin] {};
        \draw (\y, \x) node[spin] {};
      }
    \end{tikzpicture}

    \column{0.75\textwidth}

    \begin{itemize}
    \item $\Lambda$ is a finite subset of a (infinite) graph $(G,E)$.
    \item at each $u\in G$ we associate a $d$-dimensional Hilbert space $\hs_d$.
    \item a \emph{(pure) state} on $\Lambda$ is a (normalized) vector
      $\ket{\phi} \in \hs_\Lambda = \bigotimes_{u\in \Lambda} \hs_d$.
    \end{itemize}
    \pause
    If we expand $\ket{\phi}$ in the product of the standard basis, we need $d^{\abs{\Lambda}}$
    parameters.
    \[
      \ket{\phi} = \sum_{i_1, \dots, i_N=1}^d \phi_{i_1,\dots,i_N} \ket{i_1, \dots, i_N}.
    \]

  \end{columns}
\end{frame}

\begin{frame}{Tensor network states}

    Product states only require $d\abs{\Lambda}$ parameters:
    \[
      \ket{\phi} = \sum_{i_1, \dots, i_N=1}^d \phi^{(1)}_{i_1} \cdots \phi^{(N)}_{i_N} \ket{i_1, \dots, i_N}.
    \]
    \pause
    In 1D, we can generalize to \alert{matrix product states (MPS)}:
    \[
      \ket{\phi} = \sum_{i_1, \dots, i_N=1}^d \bra{L} M^{(1)}_{i_1} \cdots M^{(N)}_{i_N} \ket{R} \ket{i_1, \dots, i_N}.
    \]

    \begin{center}
    \begin{tikzpicture}

    \foreach \i in {1,...,5} {
        \node[tensor] (\i) at (\i, 0) {$M^{(\i)}$};
        \node (\i spin) at (\i, +0.7) {$i_{\i}$};
        \draw[-] (\i) -- (\i spin);
    };
    \foreach \i in {1,...,4} {
        \pgfmathtruncatemacro{\iplusone}{\i + 1};
        \draw[-] (\i) -- (\iplusone);
      };
      \node[tensor] (L) at (0,0) {$L$};
      \node[tensor] (R) at (6,0) {$R$};
      \draw[-] (L) -- (1);
      \draw[-] (5) -- (R);
    \end{tikzpicture}
  \end{center}
\end{frame}

\begin{frame}{Projected Entangled Pairs States}

  More generally: fix $D\in \field{N}$ (the \emph{bond dimension})

  \begin{columns}[T]
    \column{0.50\textwidth}
    \begin{center}
    \begin{tikzpicture}[scale=1.8]
      \draw[step=0.6cm,densely dotted] (-1.4,-1.4) grid (1.4,1.4);
      \draw<5->[-,blue!50, thick] (-1.2,1.2) -- (1.2,1.2) -- (1.2,-1.2) --
      node[midway, label={[blue]below:$X$}] {}
       (-1.2,-1.2) --  cycle;
       \draw[dashed, orange] (1,1) -- (1,-1) -- (-1,-1) -- (-1,1)
       node[midway, label={above right:$\Lambda$}] {}
        --  cycle;
      \foreach \y in {-0.6, 0, 0.6 }
        \foreach \x in {-1.2, -0.6, 0, 0, 0.6}{
          \draw<2->[epr] (\x,\y) ++(0.1,0) -- +(0.4, 0);
          \draw<2->[epr] (\y,\x) ++(0,0.1) -- +(0, 0.4);
        }
      \foreach \y in {-0.6, 0, 0.6 }
       \foreach \x in {-0.6, 0, 0, 0.6}{
          \node<3->[tensor,scale=0.8] (t\x\y) at (\x,\y) {$T$};
        }
        \foreach \y in {-0.6, 0, 0.6}
        \foreach \x in {-1.2, 1.2}{
          \node<4->[spin] (s\x\y) at (\x, \y) {};
          \node<4->[spin] (s\y\x) at (\y, \x) {};
        }

    \end{tikzpicture}
    \end{center}

    \column{0.50\textwidth}
    \pause
    \begin{itemize}[<+->]
    \item At each $e\in E_{\bar \Lambda}$ put $\ket{\omega_e} = D^{-1/2} \sum_{j=1}^D \ket{j,j}$
    \item For each $v\in \Lambda$, consider $T_v: \hs_D^{\otimes \deg(v)} \to \hs_d$ linear
    \item For $\Lambda\subset G$, denote $\hs_{\partial \Lambda} = \bigotimes_{e \in \partial
        \Lambda} \hs_D$
    \item Then for each $\ket{X} \in \hs_{\partial \Lambda}$ we can define a state in $\hs_{\Lambda}$ by
    \end{itemize}
    \begin{equation*}
      \uncover<5->{\ket{\phi_{\Lambda,X}} = \bra{X}}
      \uncover<3->{\bigotimes_{v\in \Lambda} T_v}
      \uncover<2->{\bigotimes_{e\in E_{\bar \Lambda}} \ket{\omega_e}}
    \end{equation*}
  \end{columns}
  \uncover<6>{
      \begin{alertblock}{PEPS}
        States of this form are called Projected Entangled Pair States.
      \end{alertblock}
    }
\end{frame}

\begin{frame}{PEPS}

  \alert{PEPS} are an interesting class of states:
  \begin{itemize}
  \item Efficient representation leads to good numerical algorithms
  \item Rich enough to describe entanglement structure of ground states of many-body Hamiltonians
  \item They can be naturally equipped with a \alert{local Hamiltonian} of which they are ground states
  \item Ground state degeneracy is mostly well understood
  \end{itemize}

  \begin{exampleblock}{PEPS as a theoretical tool}
    Objective: derive ``physical'' properties of the states from their tensor network description.
  \end{exampleblock}
\end{frame}

\section{Spectral gap}

\begin{frame}{Spectral gap}
  \begin{block}{Parent Hamiltonian}
    There exist a local, \alert{frustration-free} Hamiltonian $H_\Lambda = \sum_{p}h_p$, with $h_p$ orthogonal projectors, such that
    \[ h_p \ket{\phi_{\Lambda, X}} = 0 , \quad \forall X \in \hs_{\partial \Lambda}, \forall p .\]
  \end{block}
  \pause
  \begin{definition}[Spectral gap]
    The \emph{spectral gap} $\lambda(\Lambda)$ of $H_\Lambda$ is the difference between the two smallest eigenvalues
    (without multiplicities).

    We say that $H_\Lambda$ is \alert{gapped} if $\inf_{\Lambda} \lambda(\Lambda) > 0$.
  \end{definition}

\end{frame}

\begin{frame}{Spectral gap}
  Many relevant properties depend on the spectral gap:
  \begin{enumerate}
  \item Exponential decay of correlations
  \item Computational complexity of adiabatic state preparation
  \item Phase classification
  \end{enumerate}
  \pause
  At the same time is is a very hard quantity to analyze:
  \begin{enumerate}
  \item For general 2D translation invariant systems, it is an undecidable problem
    (Cubitt, Perez-Garcia, Wolf 2015)
  \item Gapless: Lieb-Schultz-Mattis theorem (1961) and higher dimensional generalizations (Hastings
    2004, Nachtergaele, Sims 2007)
  \item Positive tools: ``local gap thershold'' for 1D and some 2D systems (Knabe 1988;
    Gosset, Mozgunov 2016), ``martingale method'' (Nachtergaele 1996)
\end{enumerate}
\end{frame}

\begin{frame}{Injectivity}
  Remember: for every $\ket{X} \in \hs_{\partial \Lambda}$, we defined a state
  $\ket{\phi_{\Lambda,X}} \in \hs_{\Lambda}$. This defines a linear map
  \begin{align*}
    V_\Lambda : & \hs_{\partial \Lambda} \to \hs_{\Lambda} \\
                & \ket{X} \mapsto \ket{\phi_{\Lambda, X}}
  \end{align*}
  \pause
  \begin{definition}[injectivity]
    A PEPS is \alert{injective} on $\Lambda$ if $V_\Lambda$ is injective.
  \end{definition}
  \begin{lemma}
    If a PEPS is injective on $\Lambda_1$ and $\Lambda_2$ then it is injective on $\Lambda_1\cup
    \Lambda_2$.

    Therefore, by blocking, we will say that a PEPS is injective if it is injective on a finite volume.
  \end{lemma}
\end{frame}

\begin{frame}{The 1D case}

  \begin{theorem}[Fannes, Nachtergaele, Werner 1992, Perez-Garcia, Verstraete, Wolf, Cirac 2007]
    If a MPS is injective, then its parent Hamiltonian has a spectral gap independent of
    $\Lambda$.
  \end{theorem}

  This has led to a phase classification of 1D matrix product states.

  \begin{alertblock}{Problem:}
    In 2D, there are injective PEPS with long-range correlations, which \alert{cannot} be
    groundstates of local, gapped Hamiltonians.
  \end{alertblock}
\end{frame}

\section{Boundary states}

\begin{frame}{Boundary states of PEPS}

  \begin{definition}{Boundary state}
    For $A\subset G$ finite, let
    \[
      \rho_{\partial A} = V_A^* V_A \in \mcl B(\hs_{\partial A})
    \]
  \end{definition}

  \begin{figure}
    \includegraphics[width=0.70\textwidth]{../BndA.pdf}
  \end{figure}

\end{frame}

\begin{frame}{Properties of the boundary state}
  \[
    \rho_{\partial A} = V_A^* V_A \in \mcl B(\hs_{\partial A})
  \]
  \begin{itemize}
  \item $\rho_{\partial A} \ge 0$ (unormalized density operator)
  \item $\ker \rho_{\partial A} = \ker V_A$, so $\rho_{\partial A} > 0$ is the PEPS is injective
  \item $P_A =  V_A \rho_{\partial A}^{-1} V_A^*$ is an orthogonal projector on $\ima V_A$
  \end{itemize}
  \pause
  \begin{block}{Entanglement spectrum}
    The \alert{entanglement spectrum} is the spectrum of the reduced density matrix of a state.

    For PEPS, is equal to the spectrum of $\rho_{\partial A}^{1/2}\rho_{\partial
      A^c}\rho_{\partial A}^{1/2}$ (and in some cases, to the spectrum of $\rho_{\partial A}^2$)
  \end{block}
\end{frame}

\begin{frame}{Entanglement spectrum and Hamiltonians}
  \begin{exampleblock}{Li, Haldane 2008}
    The entanglement spectrum of a fractional quantum Hall ground state resembles a conformal
    field theory at finite temperature at the boundary.

    ``Entanglement entropy is equivalent to the thermodynamic entropy''
  \end{exampleblock}

  %FQH ground states have entanglement entropy bounded by $\abs{\partial A}$
  %(instead of $\abs{A}$), so dimension counting is correct.

  \pause

  PEPS allow to make this connection rigorous:
  \begin{definition}[Boundary Hamiltonian]
    \[ H_{\partial A} = \log \rho_{\partial A} \qq{or} \quad \rho_{\partial A} = e^{H_{\partial A}}\]
  \end{definition}

\end{frame}

\section{Interaction length and spectral gap}

\begin{frame}{The spectral gap for 2D PEPS}

\begin{theorem}[Kastoryano, L., Perez-Garcia, arxiv:1709.07691]
In 2D, if $H_{\partial A}$ has finite range for every $A$, then the parent Hamiltonian of the PEPS
is gapped.
\[ H_{\partial A} = \sum_{i} h_{[i,i+k]} \]
The same holds true if $\sum_{k} p^k \norm{h_{[i,i+k]}} < \infty$ for every $p>0$.
\end{theorem}

Details I am hiding under the carpet:
\begin{enumerate}
\item local terms at different scales are ``close'' (homogeneity)
\item normalization
\item non-injective case (finite ground state degeneracy)
\end{enumerate}
\end{frame}

\begin{frame}{Sketch of the proof}

  The parent Hamiltonian is gapped iff it has almost commuting groundstate projectors.
  \begin{lemma}[Kastoryano, L., arxiv:1705.09491]
    \[ \includegraphics[width=0.55\textwidth]{../fig3} \]
    \only<1>{
      Fix $\alpha >0$ and $\beta\in(0,1)$. Let $\ell_* = \max\{\ell_V, \ell_A, \ell_C\}$.
      If for every
    $A,B,C$ such that $\ell_B > \ell_*^\beta$, it holds that
    \[ \norm{P_{AB}P_{BC} - P_{ABC}} \le c \ell_B^{-\alpha} \]
    then $\inf \lambda(\Lambda) > 0$.}
  \only<2>{Viceversa, if the Hamiltonian is gapped, then
    \[ \norm{P_{AB}P_{BC} - P_{ABC}} \le c e^{-\frac{\ell_B}{\xi}} \]
    for some $\xi>0$.
  }
  \end{lemma}
  \vfill
\end{frame}

\begin{frame}{Ground state projectors and boundary states}
 $P_A = V_A \rho_{\partial A}^{-1} V_A$, so that $P_{AB}P_{BC}$ is equal to

  \begin{figure}
    \begin{tikzpicture}
      \foreach \i in {1,...,5} {
      \node[tensor] (A\i) at (\i, 1.5) {$T$};
      \node[tensor] (B\i) at (\i, 0)  {$T$};
      \draw  (A\i) +(2,-3) node[tensor] (C\i) {$T$};
      \draw  (B\i) +(2,-3) node[tensor] (D\i) {$T$};
      \draw[-] (A\i) -- +(0,+0.5);
      \draw[-] (B\i) -- +(0,-0.5);
      \draw[-] (C\i) -- +(0,+0.5);
      \draw[-] (D\i) -- +(0,-0.5);
    };

    \draw[boundary] (1,0.5) rectangle +(4,0.5) node[midway] (r) {$\rho_{AB}^{-1}$};
    \draw[boundary] (3,-2.5) rectangle +(4,0.5) node[midway] (s) {$\rho_{BC}^{-1}$};

    \foreach \i in {1,...,4} {
        \pgfmathtruncatemacro{\iplusone}{\i + 1};
        \draw[-] (A\i) -- (A\iplusone);
        \draw[-] (B\i) -- (B\iplusone);
        \draw[-] (C\i) -- (C\iplusone);
        \draw[-] (D\i) -- (D\iplusone);
      };

      \draw[-] (B3) -- (C1);
      \draw[-] (B4) -- (C2);
      \draw[-] (B5) -- (C3);

      \draw[-] (A1.west) to[out=180,in=180,distance=0.5cm] (1,1);
      \draw[-] (B1.west) to[out=180,in=180,distance=0.5cm] (1,0.5);
      \draw[-] (A5.east) to[out=0,in=0,distance=0.5cm] (5,1);
      \draw[-] (B5.east) to[out=0,in=0,distance=0.5cm] (5,0.5);

      \draw[-] (C1.west) to[out=180,in=180,distance=0.5cm] (3,-2);
      \draw[-] (D1.west) to[out=180,in=180,distance=0.5cm] (3,-2.5);
      \draw[-] (C5.east) to[out=0,in=0,distance=0.5cm] (7,-2);
      \draw[-] (D5.east) to[out=0,in=0,distance=0.5cm] (7,-2.5);

      \only<2>{
        \draw[boundary] (2.7, 0.3) rectangle (5.3, -1.8) node[midway] (t) {$\rho_{B}$};
      }

    \end{tikzpicture}
  \end{figure}

\end{frame}

\begin{frame}{Ground state projectors and boundary states}
 If the boundary is product, and independent of the system size.

  \begin{figure}
    \begin{tikzpicture}
      \foreach \i in {1,...,5} {
      \node[tensor] (A\i) at (\i, 1.5) {$T$};
      \draw  (\i,0) +(2,-3) node[tensor] (D\i) {$T$};
      \draw[-] (A\i) -- +(0,+0.5);
      \draw[-] (D\i) -- +(0,-0.5);
    };

    \foreach \i in {1,...,2}{
      \node[tensor] (B\i) at (\i, 0)  {$T$};
      \draw  (\i, 1.5) +(5,-3) node[tensor] (C\i) {$T$};
      \draw[-] (B\i) -- +(0,-0.5);
      \draw[-] (C\i) -- +(0,+0.5);
    }

    \foreach \i in {1,...,4} {
        \pgfmathtruncatemacro{\iplusone}{\i + 1};
        \draw[-] (A\i) -- (A\iplusone);
        \draw[-] (D\i) -- (D\iplusone);
      };

      \draw[-] (B1) -- (B2);
      \draw[-] (C1) -- (C2);

      \node[boundary] (r1) at (1,0.7) {$\rho_{L}^{-1}$};
      \draw[-] (A1.west) to[out=180, in=180] (r1.north west);
      \draw[-] (B1.west) to[out=180, in=180] (r1.south west);
      \node[boundary] (s2) at (7,-2.2) {$\rho_{R}^{-1}$};
      \draw[-] (C5.east) to[out=0, in=0] (s2.north east);
      \draw[-] (D5.east) to[out=0, in=0] (s2.south east);

      \only<1>{
      \node[boundary] (r2) at (5,0.7) {$\rho_{R}^{-1}$};
      \node[boundary] (s1) at (3,-2.2) {$\rho_{L}^{-1}$};

      \node[boundary] (t1) at (2.7,-0.7) {$\rho_{L}$};
      \node[boundary] (t2) at (5.3,-0.7) {$\rho_{R}$};

      \draw[-] (A5.east) to[out=0, in=0] (r2.north east);

      \draw[-] (D1.west) to[out=180, in=180] (s1.south west);

      \draw[-] (r2.south east) to[out=0,in=90] (t2.north);
      \draw[-] (t2.south) to[out=-90, in=180] (C1.west);

      \draw[-] (s1.north west) to[out=180,in=-90] (t1.south);
      \draw[-] (t1.north) to[out=90, in=0] (B2.east);
    }
    \only<2>{
      \draw[-] (B2.east) to[out=0,in=180] (D1.west);
      \draw[-] (A5.east) to[out=0,in=180] (C1.west);
    }

    \end{tikzpicture}
  \end{figure}

\end{frame}
\begin{frame}{Ground state projectors and boundary states}
 If the boundary is product, and independent of the system size.

  \begin{figure}
    \begin{tikzpicture}
      \foreach \i in {1,...,7} {
      \node[tensor] (A\i) at (\i, 1.5) {$T$};
      \node[tensor] (B\i) at (\i, 0)  {$T$};
      \draw[-] (A\i) -- +(0,+0.5);
      \draw[-] (B\i) -- +(0,-0.5);
    };

    \foreach \i in {1,...,6} {
        \pgfmathtruncatemacro{\iplusone}{\i + 1};
        \draw[-] (A\i) -- (A\iplusone);
        \draw[-] (B\i) -- (B\iplusone);
      };

      \node[boundary] (r1) at (1,0.7) {$\rho_{L}^{-1}$};
      \node[boundary] (r2) at (7,0.7) {$\rho_{R}^{-1}$};
      \draw[-] (A1.west) to[out=180, in=180] (r1.north west);
      \draw[-] (B1.west) to[out=180, in=180] (r1.south west);

      \draw[-] (A7.east) to[out=0, in=0] (r2.north east);
      \draw[-] (B7.east) to[out=0, in=0] (r2.south east);
    \end{tikzpicture}
  \end{figure}

  Then we have that $P_{AB}P_{CB} = P_{ABC}$ exactly.

  \begin{block}{Isometric case}
    This happens if $V_A$ is an \alert{isometry}. In this case the parent Hamiltonian is commuting
    and therefore trivially gapped.
  \end{block}

\end{frame}

\begin{frame}{Sketch of the proof}
  We want to replicate previous argument ``up to $\epsilon$''.
  \begin{lemma}
    Let $\sigma \in \mcl B(\hs_{\partial \Lambda})$ invertible, and $\tilde P_{\Lambda} = V_\Lambda
    \sigma^{-1} V_\Lambda^{*}$. Then
    \begin{align*}
      \norm{\tilde P_{\Lambda}} &= \norm{
                                  \rho_{\partial \Lambda}^{1/2} \sigma^{-1}\rho_{\partial \Lambda}^{1/2}}\\
      \norm{\tilde P_{\Lambda} - P_{\Lambda}} &=
                                                \norm{\rho_{\partial \Lambda}^{1/2} \sigma^{-1}\rho_{\partial\Lambda}^{1/2} - \mathds{1}}
     \end{align*}
   \end{lemma}
   We can generalize the previous argument if we can find operators $\sigma_{AB}$, $\sigma_{BC}$ and $\sigma_{ABC}$ such that
   \begin{enumerate}
   \item they ``approximate'' $\rho_{AB}$ , $\rho_{BC}$, $\rho_{ACB}$ (in the sense above);
   \item they ``factorize'' so that $\tilde P_{AB} \tilde P_{BC} - \tilde P_{ABC}$ is small.
   \end{enumerate}

\end{frame}

\begin{frame}{Recovering the 1D result}

  In 1D, the boundary state is the Choi–Jamiołkowski state of (a power of) the transfer operator:

  \begin{figure}
    \includegraphics[width=0.7\textwidth]{../MPSfig.pdf}
  \end{figure}

  For injective MPS, $\mathds E$ can be made to be a \alert{primitive} completely positive trace
  preserving map, and
  \[
    \mathds E^n \to \mathds E^\infty = \tr(\cdot) \sigma \qq{exponentially in $n$;}
  \]
  \[
    \norm{\rho_{\partial [1,n]} - \mathds 1 \otimes \sigma } \to 0 \qq{exponentially in $n$.}
  \]

\end{frame}

\begin{frame}{Araki's theorem}

  \begin{theorem}[Araki 1969]
    Let $Q=J\sum_{i}q_{[i,i+r]}$ a 1D $r$-local Hamiltonian, and let
    \[ \Gamma^t_\ell(f) = e^{t Q_{[-\ell, \ell]}} f e^{-t Q_{[-\ell,\ell]}},
      \qq{where}Q_{[-\ell,\ell]} = J\sum_{i:[i,i+r]\subset [-\ell,\ell]} q_{[i,i+r]}.
    \]
    Then
    \begin{align*}
      \norm{\Gamma^t_N(f) - \Gamma^t_\ell(f)} &\le \chi_\ell(\tau)F(\tau)\norm{f} ,\\
      \norm{\Gamma^t_N(f)} &\le F(\tau)\norm{f},
    \end{align*}
    uniformly in $N > \ell$,
    where $\tau = 2Jt$, $F$ depends on $\abs{\supp f}$ and $r$, $\chi_\ell$ depends on $r$.
  \end{theorem}
  The imaginary time evolution generated by $Q$ is bounded for all times $t$.
\end{frame}

\begin{frame}{Araki's theorem and expansionals}
  We can use Araki's theorem to bound the product of Gibbs states over different regions
  \begin{lemma}[Fujiwara 1952, Araki 1973]
    \[ e^{O_1} \cdots e^{O_n} =  \OExp\qty[\int_0^1 \dd t \sum_{m=1}^n \Gamma_{O_1}^t\circ \cdots \circ
      \Gamma^t_{O_{m-1}}(O_m)], \]
    where $\Gamma_O^t(f) = e^{tO} f e^{-tO}$.
  \end{lemma}

  Using these results we see that expressions of the form $e^{-H_{\partial B}} e^{H_{\partial AB}}$
  ``localize'' around $\partial A$ (with faster than exponential tails):
  \[
    e^{-H_{\partial B}}e^{H_{\partial AB}} = \OExp
    \qty[\int_0^1 \dd t \Gamma_{\partial B}^{-t}
    (H_{\partial A B})
    ] \sim e^{H_{\partial A^{+r}}} + \epsilon.
  \]
  \end{frame}

\section[Outlook]{Some numerical evidence and outlook}

\begin{frame}{Entanglement entropy}
  A number of numerical studies have been done on the entanglement entropy / entanglement
  Hamiltonian, after Li, Haldane 2008:

  \begin{itemize}
  \item Cirac, Poilblanc, Schuch, Verstraete 2011
  \item Lou, Tanaka, Katsura, Kawashima 2011
  \end{itemize}
  \begin{enumerate}
    \item AKLT model $\to$ Heisenberg ferro/anti-ferro interaction (with exponential decay)
    \item ``Ising PEPS'' $\to$ Ising interaction with transverse field
    \end{enumerate}
    Moreover, at critical points the interaction length diverges.

    \pause

    \begin{alertblock}{Exponential decay?}
      While promising, the exponential decay seen in the numerics is \alert{not} sufficient to apply
      Araki's theorem. It is also not so clear how convincingly the decay can be extrapolated (small
      system sizes).
    \end{alertblock}
\end{frame}

\begin{frame}{Outlook}

  \begin{exampleblock}{Main result}
    Finite (or super-exponential) interaction range in the boundary implies a spectral gap for the
    parent Hamiltonian of 2D PEPS.
  \end{exampleblock}
  \pause
  \begin{exampleblock}{Future work}
    \begin{enumerate}[<+->]
    \item Araki's theorem does not generalize to exponentially decaying interaction \alert{with
        growing support}. What about two-body, long-range interactions?
      \[ H = \sum_{i,j} \mu^{\abs{i-j}} {\bf S}_i \cdot {\bf S}_j \]
    \item Compute analytically the boundary state of some interesting model, i.e. the AKLT or the
      Ising PEPS.
    \item The role of temperature. For 1D AKLT, $\beta \sim 3^{-N}$.
    \end{enumerate}
  \end{exampleblock}
  \uncover<+->{
  \begin{center}
    {\Large Thank you!}
  \end{center}
  }
\end{frame}

\appendix

\section{Technical details}

\begin{frame}{Splitting the boundary}

  \begin{figure}
    \includegraphics[width=0.6\textwidth]{../bndfig2.pdf}
  \end{figure}
  \begin{align*}
    \sigma_{\partial ABC} &= \Delta_{xyb}\Delta_{axy} & \sigma_{\partial B} &=
                                                                              \Omega_{xyc}\Omega_{dxy},
    \\
    \sigma_{\partial AB} &= \Omega_{xyc}\Delta_{axy} & \sigma_{\partial BC} &= \Delta_{xyb}\Omega_{dxy};
  \end{align*}
  and
  \begin{align*}
    \Delta_{axy} &= e^{H_{ax}} e^{-H_y}e^{H_{axy}} ;\\
    \Omega_{xyc} &= e^{H_{xyc}} e^{-H_x}e^{H_{yc}} ;
  \end{align*}
  and the same for $b$, $d$.

\end{frame}

\end{document}

%%% Local Variables:
%%% coding: utf-8
%%% mode: latex
%%% TeX-engine: xetex
%%% TeX-master: t
%%% End:
